import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  ScrollView,
} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const Dropdown = ({label}) => {
  const [visible, setVisible] = useState(false);

  const toggleDropdown = () => {
    setVisible(!visible);
  };

  const renderDropdown = () => {
    if (visible) {
      return (
        <View style={{backgroundColor: 'white'}}>
          <Text style={{fontSize: 16, fontWeight: '400', color: 'black'}}>
            Signs with red circles are mostly prohibitive.
          </Text>
          <Image source={require('../images/TZone.jpg')} style={styles.image} />
          <Text style={{marginLeft: 140, color: 'black'}}>
            Entry to 20 mph zone
          </Text>
        </View>
      );
    }
  };

  return (
    <ScrollView style={styles.text}>
      <TouchableOpacity style={styles.button} onPress={toggleDropdown}>
        <Text style={styles.buttonText}>{label}</Text>
      </TouchableOpacity>

      {renderDropdown()}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  text: {
    position: 'relative',
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'lightblue',
    height: 50,
    width: '100%',
    marginVertical: 10,
    position: 'relative',
  },
  buttonText: {
    flex: 1,
    textAlign: 'center',
    color: 'white',
    fontSize: 18,
    fontWeight: '500',
  },
  dropdown: {
    position: 'absolute',
    top: 60,
    height: 100,
    width: '100%',
  },
  image: {
    width: 110,
    height: 210,
    marginTop: 20,
    marginLeft: 150,
  },
});

export default Dropdown;

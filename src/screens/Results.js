import React from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';

const Results = props => {
  const {encodedBASE64, predictions} = props.route.params.data;
  console.log('PREDICTION', predictions);
  return (
    <View style={{backgroundColor: 'white'}}>
      <Image
        source={{
          uri: `data:image/jpg;base64,${encodedBASE64}`,
        }}
        style={{width: 450, height: 550}}
      />
      <View style={{backgroundColor: 'white', height: 80}}>
        <Text
          style={{
            fontSize: 18,
            alignItems: 'center',
            alignSelf: 'center',
            marginTop: 10,
            color: 'grey',
            height: 30,
          }}>
          Entry to 20mph zone!
        </Text>
        <View>
          <Text> Predictions: {predictions.classification[0].className}</Text>
          <Text> Probability: {predictions.classification[0].probability}</Text>
        </View>
      </View>
      <View style={{backgroundColor: 'lightblue', height: 80, marginTop: 5}}>
        <Text
          style={{
            fontSize: 26,
            alignItems: 'center',
            alignSelf: 'center',
            paddingTop: 30,
          }}>
          BREAKDOWN
        </Text>
      </View>
    </View>
  );
};

export default Results;

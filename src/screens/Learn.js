import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import Dropdown from '../components/Dropdown';

const Learn = () => {
  return (
    <View style={{backgroundColor: 'white', height: '100%'}}>
      <View>
        <Text style={styles.title}> DRIVAR </Text>

        <Text style={{marginHorizontal: 10, marginTop: 20, color: 'grey'}}>
          Use your phone camera to breakdown the road sign to easily learn its
          meaning. Expand your knowledge to get you ready to drive safely.
        </Text>

        <Text style={{marginHorizontal: 10, marginTop: 20, color: 'grey'}}>
          With the use of AI, you are able to learn road signs with a click of a
          button. You could also help teach our AI with roadsigns that don't
          exist!
        </Text>
      </View>

      <Text style={styles.boldText}>Traffic Signs </Text>

      <Text
        style={{
          marginHorizontal: 10,
          marginTop: 20,
          marginBottom: 20,
          color: 'grey',
        }}>
        Traffic signs used, including signs giving orders, warning signs,
        direction signs, information signs and road works signs.
      </Text>

      <View>
        <Dropdown label="Signs Giving Orders" />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 36,
    fontWeight: '500',
    color: 'black',
    marginLeft: 130,
    marginTop: 20,
  },
  boldText: {
    fontSize: 26,
    fontWeight: '500',
    color: 'black',
    marginLeft: 120,
    marginTop: 10,
  },
});

export default Learn;

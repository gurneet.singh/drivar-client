import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Teach from '../screens/Teach';
import Learn from '../screens/Learn';
import Home from '../screens/Home';

const Tab = createBottomTabNavigator();

const TabsNavigator = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="Learn"
        component={Learn}
        options={{
          headerShown: false,
          tabBarIcon: () => (
            <MaterialCommunityIcons name="book" size={26} color="lightblue" />
          ),
        }}
      />
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          headerShown: false,
          tabBarIcon: () => (
            <MaterialCommunityIcons name="camera" size={26} color="grey" />
          ),
        }}
      />
      <Tab.Screen
        name="Teach"
        component={Teach}
        options={{
          headerShown: false,
          tabBarIcon: () => (
            <MaterialCommunityIcons name="robot" size={26} color="lightblue" />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default TabsNavigator;

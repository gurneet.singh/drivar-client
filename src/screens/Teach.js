/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, Button, TextInput, Image} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const Teach = () => {
  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <View>
        <Image
          source={require('../images/Brakes.jpg')}
          style={{
            width: 450,
            height: 410,
            alignItems: 'center',
            alignSelf: 'center',
          }}
        />

        <View>
          <TextInput style={{borderColor: 'black'}} />
        </View>

        <View style={{flexDirection: 'row', marginTop: 125}}>
          <View
            style={{
              marginLeft: 60,
              width: 200,
              alignContent: 'center',
              alignSelf: 'center',
            }}>
            <MaterialCommunityIcons name="upload" size={50} color="grey" />
          </View>
          <View
            style={{width: 200, alignContent: 'center', alignSelf: 'center'}}>
            <MaterialCommunityIcons name="camera" size={50} color="grey" />
          </View>
        </View>

        <View
          style={{
            backgroundColor: 'lightblue',
            height: 90,
            width: '100%',
          }}>
          <Text
            style={{
              fontSize: 30,
              color: 'black',
              alignContent: 'center',
              alignSelf: 'center',
              fontWeight: '500',
              marginTop: 20,
            }}>
            Teach{' '}
          </Text>
        </View>
      </View>
    </View>
  );
};

export default Teach;
